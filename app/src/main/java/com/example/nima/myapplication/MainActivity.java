package com.example.nima.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnSignUp;
    EditText txtName;
    EditText txtEMail;
    EditText txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_register_form);

        bindViews();
    }

    private void bindViews() {
        btnSignUp = (Button) findViewById(R.id.btn_SignUp);
        txtName = (EditText) findViewById(R.id.txtName);
        txtEMail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_SignUp) {
            txtName.setText("");
            txtEMail.setText("");
            txtPassword.setText("");
            Toast.makeText(this, "حساب کاربری ایجاد شد", Toast.LENGTH_LONG).show();
        }
    }
}
